local format_status_ok, format = pcall(require, "format")
if not format_status_ok then
  return
end

local lspconfig_status_ok, lspconfig = pcall(require, "lspconfig")
if not lspconfig_status_ok then
  return
end


lspconfig.gopls.setup { on_attach = require "lsp-format".on_attach }

format.setup { }

