local fn = vim.fn

-- Automatically install packer
local install_path = fn.stdpath "data" .. "/site/pack/packer/start/packer.nvim"
if fn.empty(fn.glob(install_path)) > 0 then
  PACKER_BOOTSTRAP = fn.system {
    "git", "clone", "--depth",
    "1",
    "https://github.com/wbthomason/packer.nvim",
    install_path,
  }
  print "Installing packer close and reopen Neovim..."
  vim.cmd [[packadd packer.nvim]]
end

-- Autocommand that reloads neovim whenever you save the plugins.lua file
vim.cmd [[
  augroup packer_user_config
    autocmd!
    autocmd BufWritePost plugins.lua source <afile> | PackerSync
  augroup end
]]

-- Use a protected call so we don't error out on first use
local status_ok, packer = pcall(require, "packer")
if not status_ok then
  vim.notify("Packer not found")
  return
end

-- Have packer use a popup window
packer.init {
  display = {
    open_fn = function()
      return require("packer.util").float { border = "rounded" }
    end,
  },
}


return require('packer').startup(function()
  -- Packer can manage itself
  use('wbthomason/packer.nvim')

  -- Simple plugins can be specified as strings
  use('tpope/vim-fugitive')
  use('nvim-lua/plenary.nvim')
  use "nvim-lua/popup.nvim" -- An implementation of the Popup API from vim in Neovim
  use('nvim-telescope/telescope.nvim')
  ----

  --markdown previewer
  -- install without yarn or npm
  use({
    "iamcco/markdown-preview.nvim",
    run = function() vim.fn["mkdp#util#install"]() end,
  })

  use({ "iamcco/markdown-preview.nvim", run = "cd app && npm install",
    setup = function() vim.g.mkdp_filetypes = { "markdown" } end, ft = { "markdown" }, })



  --colorschemes
  use 'folke/tokyonight.nvim'
  use { "ellisonleao/gruvbox.nvim" }
  -- cmp plugins
  use "hrsh7th/cmp-nvim-lsp"
  use "hrsh7th/cmp-buffer" -- buffer completions
  use "hrsh7th/cmp-path" -- path completions
  use "hrsh7th/cmp-cmdline" -- cmdline completions
  use "hrsh7th/nvim-cmp" -- The completion plugin 

  use "f3fora/cmp-spell" -- The spell check for completion


  -- For vsnip users.
  use "hrsh7th/cmp-vsnip"
  use "hrsh7th/vim-vsnip"


  use( "L3MON4D3/LuaSnip") --snippet engine
	use( "rafamadriz/friendly-snippets") -- a bunch of snippets to use

  use "rafamadriz/friendly-snippets" -- a bunch of snippets to use

  use {
    "windwp/nvim-autopairs",
    config = function() require("nvim-autopairs").setup {} end
  }


  use "neovim/nvim-lspconfig" -- enable LSP
  use "williamboman/nvim-lsp-installer" -- simple to use language server installer
  use "tamago324/nlsp-settings.nvim" -- language server settings defined in json for
  use "jose-elias-alvarez/null-ls.nvim" -- for formatters and linters


  use "lukas-reineke/lsp-format.nvim"


  --treesitter
  use {
    "nvim-treesitter/nvim-treesitter",
    run = ":TSUpdate"
  }
  use "p00f/nvim-ts-rainbow"

  use {
    'phaazon/hop.nvim',
    branch = 'v1', -- optional but strongly recommended
  }

  --tree plugins
  use {
    'kyazdani42/nvim-tree.lua',
    requires = {
      'kyazdani42/nvim-web-devicons', -- optional, for file icons
    },
    tag = 'nightly' -- optional, updated every week. (see issue #1193)
  }

use {
    'numToStr/Comment.nvim',
    config = function()
        require('Comment').setup()
    end
}

  --code folding
  use { 'anuvyklack/pretty-fold.nvim',
    requires = 'anuvyklack/nvim-keymap-amend', -- only for preview
    config = function()
      require('pretty-fold').setup()
      require('pretty-fold.preview').setup()
    end
  }


  use {
  'dhruvmanila/telescope-bookmarks.nvim',
  tag = '*',
  requires = {
  'kkharji/sqlite.lua',
  }
}


  use { 'heapslip/vimage.nvim' }
  --ThePrimeagen
  use 'ThePrimeagen/harpoon'
  --use 'ThePrimeagen/vim-be-good'

  use 'rhysd/vim-grammarous'

  -- Automatically set up your configuration after cloning packer.nvim
  -- Put this at the end after all plugins
  if PACKER_BOOTSTRAP then
    require("packer").sync()
  end
end)
