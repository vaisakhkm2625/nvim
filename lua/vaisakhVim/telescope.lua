
-- using extenion https://github.com/dhruvmanila/telescope-bookmarks.nvim
local telescope_status_ok, telescope = pcall(require, "telescope")
if not telescope_status_ok then
  return
end

telescope.load_extension('bookmarks')
print("bookmarks telescope")

telescope.setup {
  extensions = {
    bookmarks = {
      -- Available:
      --  * 'brave'
      --  * 'buku'
      --  * 'chrome'
      --  * 'chrome_beta'
      --  * 'edge'
      --  * 'firefox'
      --  * 'qutebrowser'
      --  * 'safari'
      --  * 'vivaldi'
      --  * 'waterfox'
      selected_browser = 'firefox',

      -- Either provide a shell command to open the URL
      url_open_command = 'open',

      -- Or provide the plugin name which is already installed
      -- Available: 'vim_external', 'open_browser'
      --url_open_plugin = nil,

      -- Show the full path to the bookmark instead of just the bookmark name
      --full_path = true,

      -- Provide a custom profile name for Firefox browser
      --firefox_profile_name = nil,

      -- Provide a custom profile name for Waterfox browser
      --waterfox_profile_name = nil,

      -- Add a column which contains the tags for each bookmark for buku
      --buku_include_tags = false,

      -- Provide debug messages
      debug = false,
    },
  }
}

