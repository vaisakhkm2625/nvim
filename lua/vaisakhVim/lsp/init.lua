local status_ok, _ = pcall(require, "lspconfig")
if not status_ok then
  return
end

require("vaisakhVim.lsp.config")
require("vaisakhVim.lsp.handlers").setup()
require("vaisakhVim.lsp.null-ls")

